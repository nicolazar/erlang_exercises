%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc The second Erlang exercise
%%% @end
%%%-------------------------------------------------------------------
-module(exercise2).

-include_lib("eunit/include/eunit.hrl").

%% API
-export([normalise/1]).

%%%-------------------------------------------------------------------
-spec(normalise(Histograms :: list(exercise1:histogram())) -> {list(), list()}).
%% @doc Given a list of histograms computed using the histogram function
%% (from exercise1), combines them into a single histogram and normalises it into
%% a distribution. Returns a tuple where the first element is the new
%% histogram and the second is the distribution as a list.
normalise(Histograms) when is_list(Histograms) ->
  %% Firstly, sort each histogram, to ensure the keys with the same value
  %% are on the same position
  SortedHistograms = lists:map(fun(H) -> lists:sort(H) end, Histograms),
  [Head | Rest] = SortedHistograms,
  normalise(Rest, Head);

normalise(_Histograms) ->
  throw(bad_argument).

%% @private
normalise([], Acc) ->
  Distrib = [Y / 100 || {_, Y} <- Acc],
  {Acc, Distrib};

normalise([Head|Rest], Acc) ->
  Acc1 = lists:zipwith(fun({H, X}, {H, Y}) -> {H, X + Y} end, Head, Acc),
  normalise(Rest, Acc1).

%% ===================================================================
%% Simple tests
%% ===================================================================
normalise_test() ->
  ?assertThrow(bad_argument, normalise(undefined)),
  ?assertThrow(bad_argument, normalise({1, 2, 3})),

  Histograms = [ [{1, 32}, {2, 20}, {3, 9}],
    [{3, 6}, {2, 15}, {1, 18}] ],
  Result = { [{1, 50}, {2, 35}, {3, 15}],
    [0.5, 0.35, 0.15]},
  ?assertEqual(Result, normalise(Histograms)).
