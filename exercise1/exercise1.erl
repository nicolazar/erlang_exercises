%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc The first Erlang exercise
%%% @end
%%%-------------------------------------------------------------------
-module(exercise1).

%% API
-export([histogram/1]).

-include_lib("eunit/include/eunit.hrl").

-type(histogram() :: list(tuple())).

-export_type([histogram/0]).

%%%-------------------------------------------------------------------
-spec(histogram(L :: list()) -> histogram()).
%% @doc Given an unordered list that contains integers between 1 and
%% 20, creates a histogram, i.e. counts the number of each value between
%% 1 and 20. The output is a proplist [{Index, Count}, ...] of length 20.
histogram(L) when is_list(L) ->
  try histogram(L, [{X, 0} || X <- lists:seq(1, 20)]) of
    Result -> Result
  catch
    Error -> throw(Error)
  end;

%% Throw an exception if the argument is not a list
histogram(_L) ->
  throw(bad_argument).

%%% @private
histogram([], Acc) ->
  Acc;

histogram([H|T], Acc) ->
  %% Lists are implemented as BIFs, so are faster than proplists;
  %% since in our case the size of the input is unknown, lists
  %% could be more suitable than proplists.
  Acc1 = case lists:keyfind(H, 1, Acc) of
    {H, Occ} -> lists:keyreplace(H, 1, Acc, {H, Occ + 1});
    false -> throw(list_item_out_of_range)
end,
  histogram(T, Acc1).

%% histogram1(L) ->
%%   histogram1(L, dict:new()).
%%
%% histogram1([], Acc) ->
%%   [{X, try dict:fetch(X, Acc) catch error:badarg -> 0 end} || X <- lists:seq(1, 20)];
%%
%% histogram1([H|T], Acc) ->
%%   Acc1 = dict:update_counter(H, 1, Acc),
%%   histogram1(T, Acc1).

%% ===================================================================
%% Simple tests
%% ===================================================================
histogram_test() ->
  ?assertThrow(bad_argument, histogram(undefined)),
  ?assertThrow(bad_argument, histogram(abc)),
  ?assertThrow(list_item_out_of_range, histogram("abc")), %% [97, 98, 99]
  ?assertThrow(list_item_out_of_range, histogram([1, 200, 3])),
  ?assertThrow(list_item_out_of_range, histogram([-1, 3, 1])),

  L = lists:seq(1, 20),
  ?assertEqual([{X, 0} || X <- L], histogram([])),
  ?assertEqual([{X, 1} || X <- L], histogram(L)),

  L1 = [5, 1, 2, 2, 4, 5, 5],
  R = [{1,1}, {2,2}, {3,0}, {4,1}, {5,3}, {6,0}, {7,0}, {8,0},
    {9,0}, {10,0}, {11,0}, {12,0}, {13,0}, {14,0}, {15,0},
    {16,0}, {17,0}, {18,0}, {19,0},{20,0}],
  ?assertEqual(R, histogram(L1)),

  ?assertMatch([{1, 1} | _], histogram([1])).
