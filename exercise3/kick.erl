%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%%-------------------------------------------------------------------
-module(kick).

%% API
-export([do_something/0]).

%% Helper method
do_something() ->
  io:format("Just did something at ~p...~n", [calendar:now_to_local_time(os:timestamp())]).
