%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc The third Erlang exercise
%%% @end
%%%-------------------------------------------------------------------
-module(timer).

-behaviour(gen_server).

%% API
-export([start_link/0,
  stop/0,
  timings_test_cast/0,
  timings_test_call/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).
-define(TIMEOUT, 60 * 60 * 1000).

%% The state of the server.
%% Contains a single field that specifies the last time
%% a timeout occurred.
-record(state, {timeout_ms :: non_neg_integer()}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

%%--------------------------------------------------------------------
%% @doc
%% Stops the server
%% @end
%%--------------------------------------------------------------------
-spec(stop() -> ok).
stop() ->
  gen_server:call(?MODULE, stop).

%%--------------------------------------------------------------------
%% @doc
%% A cast message sent to the server to verify that the timings
%% are not affected by random messages.
%% @end
%%--------------------------------------------------------------------
-spec(timings_test_cast() -> ok).
timings_test_cast() ->
  gen_server:cast(?MODULE, timings_test_cast).

%%--------------------------------------------------------------------
%% @doc
%% A call message sent to the server to verify that the timings
%% are not affected by random messages.
%% @end
%%--------------------------------------------------------------------
-spec(timings_test_call() -> ok).
timings_test_call() ->
  gen_server:call(?MODULE, timings_test_call).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init([]) ->
  State = #state{timeout_ms = now_ms()},
  {ok, State, ?TIMEOUT}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages.
%% Make sure that the periodically task is always executed at the
%% specified time. Update the timeout accordingly (if some delta time
%% passed from the last execution time, the new timeout should substract
%% that time).
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(stop, _From, State) ->
  {stop, normal, State};
handle_call(Request, _From, State = #state{timeout_ms = LastTimeoutMs}) ->
  io:format("Received call request ~p", [Request]),
  DeltaTime = now_ms() - LastTimeoutMs,
  {reply, ok, State, ?TIMEOUT - DeltaTime}.


%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages.
%% Make sure that the periodically task is always executed at the
%% specified time.
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast(Request, State = #state{timeout_ms = LastTimeoutMs}) ->
  io:format("Unhandled cast request ~p", [Request]),
  DeltaTime = now_ms() - LastTimeoutMs,
  {noreply, State, ?TIMEOUT - DeltaTime}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%% When the timeout occurs, execute the method from the other module
%% and update the state.
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(timeout, State = #state{}) ->
  kick:do_something(),
  {noreply, State#state{timeout_ms = now_ms()}, ?TIMEOUT}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
now_ms() ->
  now_ms(os:timestamp()).

now_ms({Mega, Sec, Micro}) ->
  (Mega * 1000000000) + (Sec * 1000) + (Micro div 1000).
