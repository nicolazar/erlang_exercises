%%%-------------------------------------------------------------------
%% @doc weather app public API
%% @end
%%%-------------------------------------------------------------------

-module('weather_app').

-behaviour(application).

%% Application callbacks
-export([start/2
        ,stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
  ensure_started(lager),
  ensure_started(cowboy),
  'weather_sup':start_link().

stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
ensure_started(App) ->
  io:format("Starting app ~p", [App]),
  case application:start(App) of
    ok -> ok;
    {error, {already_started, App}} -> ok
  end.
