%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc Worker module. Collects temperatures readings, computes
%%% histograms and distributions and allows the retrieval of the last
%%% calculated distribution.
%%% @end
%%%-------------------------------------------------------------------
-module(weather_worker).

-behaviour(gen_server).

%% API
-export([start_link/1,
         stop/0,
         submit/2,
         retrieve/0]).

%% gen_server callbacks
-export([init/1,
  handle_call/3,
  handle_cast/2,
  handle_info/2,
  terminate/2,
  code_change/3]).

-define(SERVER, ?MODULE).

-define(DEFAULT_TIMEOUT, 1). %% In minutes

%% Helper macro definition
-define(GET_TIMEOUT(State),
  State#state.computation_timeout - (weather_util:now_ms() - State#state.last_computed_time)).

-record(state, {
    distribution :: tuple(), %% the hourly distribution
    last_computed_time :: non_neg_integer(), %% the last time of the computation
    computation_timeout :: non_neg_integer(), %% the timeout between computations
    temperatures = [] :: list(list()) %% collection of temperatures lists
  }).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(start_link(MinBtwComp :: non_neg_integer()) ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link(MinBtwComp) ->
  gen_server:start_link({local, ?SERVER}, ?MODULE, MinBtwComp, []).

%%--------------------------------------------------------------------
%% @doc
%% Stops the server
%%
%% @end
%%--------------------------------------------------------------------
-spec(stop() -> ok).
stop() ->
  gen_server:call(?MODULE, stop).

%%--------------------------------------------------------------------
%% @doc
%% Collects the submitted temperatures from the StationId.
%% @end
%%--------------------------------------------------------------------
-spec(submit(StationId :: string(), TempList :: list()) -> ok).
submit(StationId, TempList) ->
  gen_server:cast(?MODULE, {new_submission, [StationId, TempList]}).

%%--------------------------------------------------------------------
%% @doc
%% Returns the last computed distribution.
%% @end
%%--------------------------------------------------------------------
-spec(retrieve() -> {ok, Distribution :: tuple()}).
retrieve() ->
  gen_server:call(?MODULE, retrieve_distribution).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
-spec(init(Args :: term()) ->
  {ok, State :: #state{}} | {ok, State :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term()} | ignore).
init(MinBtwComp) ->
  Timeout = case is_number(MinBtwComp) of
    true -> abs(MinBtwComp * 60 * 1000); %% ensure positive timeout
    false -> ?DEFAULT_TIMEOUT * 60 * 1000
  end,

  {ok, #state{distribution = undefined,
              last_computed_time = weather_util:now_ms(),
              computation_timeout = Timeout}, Timeout}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_call(Request :: term(), From :: {pid(), Tag :: term()},
    State :: #state{}) ->
  {reply, Reply :: term(), NewState :: #state{}} |
  {reply, Reply :: term(), NewState :: #state{}, timeout() | hibernate} |
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), Reply :: term(), NewState :: #state{}} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_call(retrieve_distribution, _From, #state{distribution = Distrib} = State) ->
  lager:log(notice, ?MODULE, "Retrieving the distribution from the state... ~n", []),
  {reply, {ok, Distrib}, State, ?GET_TIMEOUT(State)};
handle_call(stop, _From, State) ->
  {stop, normal, State};
handle_call(Request, _From, State) ->
  lager:log(info, ?MODULE, "Unhandled call request ~p", [Request]),
  {reply, ok, State, ?GET_TIMEOUT(State)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
-spec(handle_cast(Request :: term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_cast({new_submission, [_StationId, NewTemps]}, #state{temperatures = Temperatures} = State) ->
  {noreply, State#state{temperatures = [NewTemps | Temperatures]}, ?GET_TIMEOUT(State)};
handle_cast(Request, State) ->
  lager:log(info, ?MODULE, "Unhandled cast request ~p", [Request]),
  {noreply, State, ?GET_TIMEOUT(State)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
  {noreply, NewState :: #state{}} |
  {noreply, NewState :: #state{}, timeout() | hibernate} |
  {stop, Reason :: term(), NewState :: #state{}}).
handle_info(timeout, #state{temperatures = Temperatures} = State) ->
  NewDistrib = distribution_handler:compute_distribution(Temperatures),
  lager:log(notice, ?MODULE, "Time to compute new distribution, got result ~p", [NewDistrib]),
  NewState = State#state{distribution = NewDistrib,
                         temperatures = [],
                         last_computed_time = weather_util:now_ms()},
  {noreply, NewState, ?GET_TIMEOUT(NewState)};
handle_info(_Info, State) ->
  {noreply, State, ?GET_TIMEOUT(State)}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, _State) ->
  ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
-spec(code_change(OldVsn :: term() | {down, term()}, State :: #state{},
    Extra :: term()) ->
  {ok, NewState :: #state{}} | {error, Reason :: term()}).
code_change(_OldVsn, State, _Extra) ->
  {ok, State}.
