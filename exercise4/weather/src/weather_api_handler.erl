%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc Requests handler
%%% @end
%%%-------------------------------------------------------------------
-module(weather_api_handler).

%% API
-export([init/3,
  handle/2,
  terminate/3]).

init({_, http}, Req, _Opts) ->
  {ok, Req, undefined}.

handle(Request, State) ->
  case cowboy_req:method(Request) of
    {<<"POST">>, Req2} ->
      handle_post(Req2, State);
    {<<"GET">>, Req2} ->
      handle_get(Req2, State);
    _Other ->
      {ok, Req2} = cowboy_req:reply(405, Request),
      {ok, Req2, State}
  end.

handle_post(Request, State) ->
  {ok, BodyQs, Req2} = cowboy_req:body_qs(Request),

  StationId = proplists:get_value(<<"station_id">>, BodyQs),
  TempList = proplists:get_value(<<"temperatures">>, BodyQs),

  weather_worker:submit(StationId, TempList),
  Response = {[{result, success}]},
  ResponseBin = jiffy:encode(Response),
  {ok, Req3} = cowboy_req:reply(200, [],  ResponseBin, Req2),
  {ok, Req3, State}.

handle_get(Request, State) ->
  Result = weather_worker:retrieve(),
  Response = {[{result, Result}]},
  ResponseBin = jiffy:encode(Response),
  {ok, Req2} = cowboy_req:reply(200, [],  ResponseBin, Request),
  {ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
  ok.









