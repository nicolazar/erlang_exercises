%%%-------------------------------------------------------------------
%% @doc weather top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('weather_sup').

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-define(DEFAULT_MAX_RESTART, 5).
-define(DEFAULT_MAX_TIME, 60).
-define(DEFAULT_COMP_TIME, 1).

%% Children specification - a tuple composed by:
%% {ChildId, StartFuncion :: {M, F, A}, Restart, Shutdown, Type, Modules}
-define(CHILD_SPEC(Mod, Type, Args),
  {Mod, {Mod, start_link, Args}, permanent, 5000, Type, [Mod]}).

%%====================================================================
%% API functions
%%====================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the supervisor
%% @end
%%--------------------------------------------------------------------
-spec(start_link() ->
  {ok, Pid :: pid()} | ignore | {error, Reason :: term()}).
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
  ComputeOnline = application:get_env(weather, compute_online, true),
  MinBtwComp = application:get_env(weather, minutes_between_computations, ?DEFAULT_COMP_TIME),

  lager:log(info, ?MODULE, "Initialise supervisor, minutes between computations ~p", [MinBtwComp]),
  ChildSpecs = case ComputeOnline of
                 false -> [?CHILD_SPEC(weather_worker, worker, [MinBtwComp])];
                 true  -> [?CHILD_SPEC(weather_worker_online, worker, [MinBtwComp])]
               end,

  WebOpts = application:get_env(weather, cowboy_config),
  WebWorkerSpec = [?CHILD_SPEC(weather_web, worker, [WebOpts])],

  AllSpecs = ChildSpecs ++ WebWorkerSpec,
  {ok, {
    {one_for_all, ?DEFAULT_MAX_RESTART, ?DEFAULT_MAX_TIME},
    AllSpecs
    }
  }.
%%====================================================================
%% Internal functions
%%====================================================================
