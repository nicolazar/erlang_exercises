%%%-------------------------------------------------------------------
%%% @author Nicoleta Lazar
%%% @doc Handles distributions calculations
%%% @end
%%%-------------------------------------------------------------------
-module(distribution_handler).

%% API
-export([compute_distribution/1,
         update_current_distribution/2]).

-type(histogram() :: list(tuple())).

-define(MIN_TEMP, 0). %% Minimum possible temperature
-define(MAX_TEMP, 20). %% Maximum possible temperature

%%%-------------------------------------------------------------------
-spec(compute_distribution(Temperatures :: list()) -> tuple()).
%% @doc Given a list of temperatures lists, compute the normalized distribution
%% of all temperatures.
compute_distribution(Temperatures) when is_list(Temperatures) andalso length(Temperatures) > 0 ->
  Histograms = lists:map(
    fun(T) ->
      try histogram(T) catch throw:_Error -> undefined end
    end,
    Temperatures),
  Filtered = [H || H <- Histograms, H =/= undefined], %% throw away bad data
  Result = try normalise(Filtered) catch throw:Error ->
    lager:log(error, ?MODULE, "An error occurred when computing the distribution: ~p", [Error]),
    undefined end,
  Result;

compute_distribution(_Temperatures) -> undefined.

%%%-------------------------------------------------------------------
%% @doc Given a list of temperatures and the last computed distribution,
%% update the distribution.
-spec(update_current_distribution(Temperatures :: list(), CurrentDistrib :: tuple()) -> tuple()).
update_current_distribution(Temperatures, CurrentDistrib) ->
  case CurrentDistrib of
    undefined ->
      normalise([histogram(Temperatures)]);
    _ ->
      {Hist, _Distrib} = CurrentDistrib,
      normalise([histogram(Temperatures)], Hist)
  end.

%%%===================================================================
%%% Internal functions
%%%===================================================================

%%%-------------------------------------------------------------------
-spec(histogram(L :: list()) -> histogram()).
%% @doc Given an unordered list that contains temperatures, creates a
%% histogram, i.e. counts the number of each value between
%% 'MinTemp' and 'MaxTemp'. The output is a proplist [{Index, Count}, ...]
%% of length ('MaxTemp' - 'MinTemp' + 1).
%% @end
histogram(L) when is_list(L) andalso length(L) > 0 ->
  try histogram(L, [{X, 0} || X <- lists:seq(?MIN_TEMP, ?MAX_TEMP)]) of
    Result -> Result
  catch
    Error -> throw(Error)
  end;

histogram(_L) ->
  throw(bad_argument).

%%% @private
histogram([], Acc) ->
  Acc;

histogram([H|T], Acc) ->
  %% Lists are implemented as BIFs, so are faster than proplists;
  %% since in our case the size of the input is unknown, lists
  %% could be more suitable than proplists.
  Acc1 = case lists:keyfind(H, 1, Acc) of
           {H, Occ} -> lists:keyreplace(H, 1, Acc, {H, Occ + 1});
           false -> throw(list_item_out_of_range)
         end,
  histogram(T, Acc1).

%%%-------------------------------------------------------------------
-spec(normalise(Histograms :: list(histogram())) -> {list(), list()}).
%% @doc Given a list of histograms computed using the histogram function,
%% combines them into a single histogram and normalises it into
%% a distribution. Returns a tuple where the first element is the new
%% histogram and the second is the distribution as a list.
normalise(Histograms) ->
  %% First sort each histogram, to ensure the keys with the same value
  %% are on the same position
  SortedHistograms = lists:map(fun(H) -> lists:sort(H) end, Histograms),
  [Head | Rest] = SortedHistograms,
  normalise(Rest, Head).

%% @private
normalise([], Acc) ->
  Distrib = [Y / 100 || {_, Y} <- Acc],
  {Acc, Distrib};

normalise([Head|Rest], Acc) ->
  Acc1 = lists:zipwith(fun({H, X}, {H, Y}) -> {H, X + Y} end, Head, Acc),
  normalise(Rest, Acc1).

